﻿using FluentAssertions;
using RatDoto.WebApiClient.Responses;
using Xunit;

namespace RatDoto.WebApiClient.Tests.Services.Dota.Matches
{
    public class GetLeagueListingsTests
    {
        private readonly IDotaApi _dotaApi;

        public GetLeagueListingsTests()
        {
            _dotaApi = DotaApiFactory.Create();
        }

        [Fact]
        public void ShouldReturnAllLeagues()
        {
            var result = _dotaApi.GetLeagueListings().Execute();

            result.Should().NotBeNull().And.BeOfType<DotaLeagues>();
            result.Leagues.Should().NotBeEmpty();
        }
    }
}
