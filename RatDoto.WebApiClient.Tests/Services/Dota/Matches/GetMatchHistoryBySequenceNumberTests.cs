﻿using FluentAssertions;
using Xunit;

namespace RatDoto.WebApiClient.Tests.Services.Dota.Matches
{
    public class GetMatchHistoryBySequenceNumberTests
    {
        private readonly IDotaApi _dotaApi;

        public GetMatchHistoryBySequenceNumberTests()
        {
            _dotaApi = DotaApiFactory.Create();
        }

        [Fact]
        public void ShouldReturnDefaultResultsIfNothingSpecified()
        {
            var result = _dotaApi.GetMatchHistoryBySequenceNumber().Execute();

            result.Matches.Should().HaveCount(100);
        }

        [Fact]
        public void ShouldReturnLimitedNumberOfResults()
        {
            const int numberOfResults = 7;
            var result = _dotaApi.GetMatchHistoryBySequenceNumber().Take(numberOfResults).Execute();

            result.Matches.Should().HaveCount(numberOfResults);
        }

        [Fact]
        public void ShouldReturnResultsStartingWithSequenceNumber()
        {
            const int sequenceNumber = 1000000;
            var result = _dotaApi.GetMatchHistoryBySequenceNumber().StartingAt(sequenceNumber).Execute();

            result.Matches.Should().ContainSingle(x => x.MatchSequenceNumber == sequenceNumber)
                               .And.OnlyContain(x => x.MatchSequenceNumber >= sequenceNumber);
        }
    }
}
