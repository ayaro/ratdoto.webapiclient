﻿using System;
using FluentAssertions;
using RatDoto.WebApiClient.Responses;
using Xunit;

namespace RatDoto.WebApiClient.Tests.Services.Dota.Matches
{
    public class GetMatchDetailsServiceTests
    {
        private readonly IDotaApi _dotaApi;

        public GetMatchDetailsServiceTests()
        {
            _dotaApi = DotaApiFactory.Create();
        }

        [Fact]
        public void ShouldReturnMatchDetailsForSpecifiedMatch()
        {
            const int requestedMatchId = 271145478;

            var result = _dotaApi.GetMatchDetails().ForMatch(requestedMatchId).Execute();

            result.Should().NotBeNull().And.BeOfType<MatchDetails>();
            result.Id.Should().Be(requestedMatchId);
        }

        [Fact]
        public void ShouldThrowIfMatchIdNotSpecified()
        {
            _dotaApi.Invoking(x => x.GetMatchDetails().Execute()).ShouldThrow<InvalidOperationException>();
        }
    }
}
