﻿using FluentAssertions;
using RatDoto.WebApiClient.Responses;
using Xunit;

namespace RatDoto.WebApiClient.Tests.Services.Dota.Economy
{
    public class GetHeroesServiceTests
    {
        private readonly IDotaApi _dotaApi;

        public GetHeroesServiceTests()
        {
            _dotaApi = DotaApiFactory.Create();
        }

        [Fact]
        public void ShouldReturnAllGameHeroes()
        {
            var result = _dotaApi.GetHeroes().Execute();

            result.Should().NotBeNull().And.BeOfType<DotaHeroes>();
            result.Heroes.Should().HaveCount(x => x > 100).And.HaveCount(result.Count);
        }

        [Fact]
        public void ShouldReturnItemizedOnlyHeroes()
        {
            var allHeroes = _dotaApi.GetHeroes().Execute();
            var itemizedHeroes = _dotaApi.GetHeroes().ItemizedOnly().Execute();

            itemizedHeroes.Count.Should().BeLessThan(allHeroes.Count);
        }
    }
}
