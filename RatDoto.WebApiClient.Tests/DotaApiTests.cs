﻿using System;
using FluentAssertions;
using RatDoto.WebApiClient.Responses;
using Xunit;

namespace RatDoto.WebApiClient.Tests
{
    public class DotaApiTests
    {
        [Fact]
        public void Smoke()
        {
            var dotaApi = DotaApiFactory.Create();
            var result = dotaApi.GetMatchHistory().From(DateTime.Today).Execute();

            result.Should().NotBeNull().And.BeOfType<MatchHistory>();
            result.ShouldBeEquivalentTo(new { TotalNumberOfResults = 500 }, cfg => cfg.ExcludingMissingProperties());
        }

        [Fact]
        public void Smoke2()
        {
            var dotaApi = DotaApiFactory.Create();
            var result = dotaApi.GetMatchHistoryBySequenceNumber().Take(10).Execute();

            result.Should().NotBeNull().And.BeOfType<MatchHistoryBySequenceNumber>();
            result.Matches.Should().HaveCount(10);
        }

        [Fact]
        public void Smoke3()
        {
            var dotaApi = DotaApiFactory.Create();
            var result = dotaApi.GetMatchDetails().ForMatch(271145478).Execute();

            result.Should().NotBeNull().And.BeOfType<MatchDetails>();
        }

        [Fact]
        public void Smoke4()
        {
            var dotaApi = DotaApiFactory.Create();
            var result = dotaApi.GetHeroes().ItemizedOnly().Execute();

            result.Should().NotBeNull().And.BeOfType<DotaHeroes>();
        } 
    }
}
