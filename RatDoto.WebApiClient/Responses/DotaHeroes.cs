﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
    [DataContract]
    public class DotaHeroes
    {
        [DataMember(Name = "heroes")]
        public Hero[] Heroes { get; set; }

        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}
