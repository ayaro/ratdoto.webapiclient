﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class MatchHistory
	{
		[DataMember(Name = "status")]
        public ResultStatus Status { get; set; }
		
		[DataMember(Name = "num_results")]
		public int CurrentNumberOfResults { get; set; }
		
		[DataMember(Name = "total_results")]
		public int TotalNumberOfResults { get; set; }
		
		[DataMember(Name = "results_remaining")]
		public int NumberOfResultsLeft { get; set; }

		[DataMember(Name = "matches")]
        public Match[] Matches { get; set; }

        public enum ResultStatus
        {
            Undefined = 0,
            Success = 1,
            ForbiddenByUser = 15
        }
	}
}