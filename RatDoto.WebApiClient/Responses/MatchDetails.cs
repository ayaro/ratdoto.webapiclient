﻿using System;
using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class MatchDetails
	{
	    [DataMember(Name = "players")]
	    public PlayerDetails[] Players { get; set; }

	    [DataMember(Name = "radiant_win")]
		public bool DidRadiantWin { get; set; }

		[DataMember(Name = "duration")]
		public int DurationInSeconds { get; set; }

		[DataMember(Name = "start_time")]
		public DateTime StartTime { get; set; }

		[DataMember(Name = "match_id")]
		public int Id { get; set; }

		[DataMember(Name = "match_seq_num")]
		public int MatchSequenceNumber { get; set; }

		[DataMember(Name = "tower_status_radiant")]
		public TowerStatus TowerStatusRadiant { get; set; }

		[DataMember(Name = "tower_status_dire")]
		public TowerStatus TowerStatusDire { get; set; }

		[DataMember(Name = "barracks_status_radiant")]
		public BarracksStatus BarracksStatusRadiant { get; set; }

		[DataMember(Name = "barracks_status_dire")]
		public BarracksStatus BarracksStatusDire { get; set; }

		[DataMember(Name = "cluster")]
		public int Cluster { get; set; }

		[DataMember(Name = "first_blood_time")]
		public int FirstBloodTimeInSeconds { get; set; }

		[DataMember(Name = "lobby_type")]
		public LobbyType LobbyType { get; set; }

		[DataMember(Name = "human_players")]
		public int HumanPlayers { get; set; }

		[DataMember(Name = "leagueid")]
		public int LeagueId { get; set; }

		[DataMember(Name = "positive_votes")]
		public int PositiveVotes { get; set; }

		[DataMember(Name = "negative_votes")]
		public int NegativeVotes { get; set; }

		[DataMember(Name = "game_mode")]
		public GameMode GameMode { get; set; }
	}
}