﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class PlayerDetails
	{
		[DataMember(Name = "account_id")]
		public long Id { get; set; }

		[DataMember(Name = "player_slot")]
		public byte Slot { get; set; }

		[DataMember(Name = "hero_id")]
		public int HeroId { get; set; }

		[DataMember(Name = "item_0")]
		public int FirstSlotItemId { get; set; }

		[DataMember(Name = "item_1")]
		public int SecondSlotItemId { get; set; }

		[DataMember(Name = "item_2")]
		public int ThirdSlotItemId { get; set; }

		[DataMember(Name = "item_3")]
		public int FourthSlotItemId { get; set; }

		[DataMember(Name = "item_4")]
		public int FifthSlotItemId { get; set; }

		[DataMember(Name = "item_5")]
		public int SixthSlotItemId { get; set; }

		[DataMember(Name = "kills")]
		public int Kills { get; set; }

		[DataMember(Name = "deaths")]
		public int Deaths { get; set; }

		[DataMember(Name = "assists")]
		public int Assists { get; set; }

		[DataMember(Name = "leaver_status")]
		public LeaverStatus? LeaverStatus { get; set; }

		[DataMember(Name = "gold")]
		public int Gold { get; set; }

		[DataMember(Name = "last_hits")]
		public int LastHits { get; set; }

		[DataMember(Name = "denies")]
		public int Denies { get; set; }

		[DataMember(Name = "gold_per_min")]
		public int GoldPerMinute { get; set; }

		[DataMember(Name = "xp_per_min")]
		public int ExperiencePerMinute { get; set; }

		[DataMember(Name = "gold_spent")]
		public int GoldSpent { get; set; }

		[DataMember(Name = "hero_damage")]
		public int HeroDamage { get; set; }

		[DataMember(Name = "tower_damage")]
		public int TowerDamage { get; set; }

		[DataMember(Name = "hero_healing")]
		public int HeroHealing { get; set; }

		[DataMember(Name = "level")]
		public int Level { get; set; }

		[DataMember(Name = "ability_upgrades")]
		public AbilityUpgrade[] AbilityUpgrades { get; set; }

		[DataMember(Name = "additional_units")]
		public AdditionalUnit[] AdditionalUnits { get; set; }
	}
}