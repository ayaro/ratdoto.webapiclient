﻿using System;

namespace RatDoto.WebApiClient.Responses
{
	[Flags]
	public enum BarracksStatus : byte
	{
		TopMelee = 1,
		TopRanged = 2,
		MiddleMelee = 4,
		MiddleRanged = 8,
		BottomMelee = 16,
		BottomRanged = 32
	}
}