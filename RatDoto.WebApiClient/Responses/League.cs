﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
    [DataContract]
    public class League
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "leagueid")]
        public int Id { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "tournament_url")]
        public string Url { get; set; }
    }
}