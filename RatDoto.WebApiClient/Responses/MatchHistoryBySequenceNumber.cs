﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
    public class MatchHistoryBySequenceNumber
	{
		[DataMember(Name = "status")]
        public ResultStatus Status { get; set; }
		
		[DataMember(Name = "statusDetail")]
		public string StatusDetails { get; set; }

		[DataMember(Name = "matches")]
		public MatchDetails[] Matches { get; set; }

        public enum ResultStatus
        {
            Undefined = 0,
            Success = 1,
            MatchesRequestedMustBeGreaterThenZero = 8
        }
	}
}