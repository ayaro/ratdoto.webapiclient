﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
    [DataContract]
    public class DotaLeagues
    {
        [DataMember(Name = "leagues")]
        public League[] Leagues { get; set; }
    }
}