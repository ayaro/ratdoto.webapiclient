﻿namespace RatDoto.WebApiClient.Responses
{
	public enum LeaverStatus
	{
		StayedForEntireMatch = 0,
		SafeLeave = 1,
		Abandoned = 2
	}
}