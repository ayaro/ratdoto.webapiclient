﻿using System;
using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class Match
	{
		[DataMember(Name = "match_id")]
		public long Id { get; set; }

		[DataMember(Name = "match_seq_num")]
        public long SequenceNumber { get; set; }

		[DataMember(Name = "start_time")]
		public DateTime StartTime { get; set; }

		[DataMember(Name = "lobby_type")]
		public LobbyType LobbyType { get; set; }

		[DataMember(Name = "players")]
		public Player[] Players { get; set; }
	}
}