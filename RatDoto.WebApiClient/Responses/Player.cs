﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class Player
	{
		[DataMember(Name = "account_id")]
		public long Id { get; set; }

		[DataMember(Name = "player_slot")]
		public byte Slot { get; set; }

		[DataMember(Name = "hero_id")]
		public int HeroId { get; set; }
	}
}