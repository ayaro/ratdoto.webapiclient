using System;
using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class AbilityUpgrade
	{
		[DataMember(Name = "ability")]
		public int Id { get; set; }

		[DataMember(Name = "time")]
		public DateTime SpentAtTimeInSeconds { get; set; }

		[DataMember(Name = "level")]
		public int SpentOnLevel { get; set; }
	}
}