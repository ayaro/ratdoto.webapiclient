using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class AdditionalUnit
	{
		[DataMember(Name = "unitname")]
		public string Name { get; set; }

		[DataMember(Name = "item_0")]
		public int FirstSlotItemId { get; set; }

		[DataMember(Name = "item_1")]
		public int SecondSlotItemId { get; set; }

		[DataMember(Name = "item_2")]
		public int ThirdSlotItemId { get; set; }

		[DataMember(Name = "item_3")]
		public int FourthSlotItemId { get; set; }

		[DataMember(Name = "item_4")]
		public int FifthSlotItemId { get; set; }

		[DataMember(Name = "item_5")]
		public int SixthSlotItemId { get; set; }
	}
}