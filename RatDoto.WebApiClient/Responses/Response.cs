﻿using System.Runtime.Serialization;

namespace RatDoto.WebApiClient.Responses
{
	[DataContract]
	public class Response<TResult>
	{
		[DataMember(Name = "result")]
		public TResult Result { get; set; }
	}
}
