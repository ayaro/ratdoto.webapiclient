﻿namespace RatDoto.WebApiClient.Responses
{
	public enum LobbyType
	{
		Invalid = -1,
		PublicMatchmaking = 0,
		Practice = 1,
		Tournament = 2,
		Tutorial = 3,
		CoopWithBots = 4,
		TeamMatch = 5,
		SoloQueue = 6,
        Ranked = 7,
        SoloMid = 8
	}
}