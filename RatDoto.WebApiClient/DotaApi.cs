﻿using System;
using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Fluent.Concrete;
using RatDoto.WebApiClient.Queries;

namespace RatDoto.WebApiClient
{
    internal class DotaApi : IDotaApi
    {
        private readonly IQueryProcessor _queryProcessor;

        public DotaApi(IQueryProcessor queryProcessor)
        {
            if (null == queryProcessor)
            {
                throw new ArgumentNullException("queryProcessor");
            }

            _queryProcessor = queryProcessor;
        }

        public IGetMatchHistoryQueryBuilder GetMatchHistory()
        {
            return new GetMatchHistoryQueryBuilder(_queryProcessor);
        }

        public IGetMatchHistoryBySequenceNumberQueryBuilder GetMatchHistoryBySequenceNumber()
        {
            return new GetMatchHistoryBySequenceNumberQueryBuilder(_queryProcessor);
        }

        public IGetMatchDetailsQueryBuilder GetMatchDetails()
        {
            return new GetMatchDetailsQueryBuilderQueryBuilder(_queryProcessor);
        }

        public IGetHeroesQueryBuilder GetHeroes()
        {
            return new GetHeroesQueryBuilder(_queryProcessor);
        }

        public IGetLeagueListingsQueryBuilder GetLeagueListings()
        {
            return new GetLeagueListingsQueryBuilder(_queryProcessor);
        }
    }
}