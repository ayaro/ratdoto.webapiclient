﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Engine
{
	internal class WebClientWrapper : IWebClient
	{
		private readonly WebClient _webClient;

		public WebClientWrapper(WebClient webClient)
		{
			if (webClient == null) throw new ArgumentNullException("webClient");

			_webClient = webClient;
		}

		public Task<Stream> LoadStream(Uri uri)
		{
			return _webClient.OpenReadTaskAsync(uri);
		}
	}
}
