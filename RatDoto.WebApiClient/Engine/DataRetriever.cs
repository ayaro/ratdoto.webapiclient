﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;
using RatDoto.WebApiClient.Engine.Transformation;

namespace RatDoto.WebApiClient.Engine
{
    internal class DataRetriever : IDataRetriever
    {
        private readonly RequestUriBuilder _requestUriBuilder;
        private readonly IWebClient _webClient;
        private readonly IParser _parser;

        public DataRetriever(RequestUriBuilder requestUriBuilder, IWebClient webClient, IParser parser)
        {
            if (null == requestUriBuilder)
            {
                throw new ArgumentNullException("requestUriBuilder");
            }
            if (null == webClient)
            {
                throw new ArgumentNullException("webClient");
            }
            if (null == parser)
            {
                throw new ArgumentNullException("parser");
            }

            _requestUriBuilder = requestUriBuilder;
            _webClient = webClient;
            _parser = parser;
        }

        public async Task<TResponse> GetResponse<TResponse>(string relativePath, NameValueCollection parameters)
        {
            Uri uri = _requestUriBuilder.BuildRequestUri(relativePath, parameters);
            Stream responseStream = await _webClient.LoadStream(uri);
            var response = _parser.Parse<TResponse>(responseStream);

            return response;
        }
    }
}