﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Engine
{
	public interface IWebClient
	{
		Task<Stream> LoadStream(Uri uri);
	}
}