﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace RatDoto.WebApiClient.Engine.Transformation
{
	internal class JsonParser : IParser
	{
		private readonly JsonSerializer _jsonSerializer;

		public JsonParser(JsonSerializer jsonSerializer)
		{
			if (jsonSerializer == null) throw new ArgumentNullException("jsonSerializer");

			_jsonSerializer = jsonSerializer;
		}

		public TResult Parse<TResult>(Stream stream)
		{
			using (var streamReader = new StreamReader(stream))
			{
				using (var jsonReader = new JsonTextReader(streamReader))
				{
					return _jsonSerializer.Deserialize<TResult>(jsonReader);
				}
			}
		}
	}
}