﻿using System;
using Newtonsoft.Json;
using RatDoto.WebApiClient.Common.Extensions;

namespace RatDoto.WebApiClient.Engine.Transformation
{
	internal class UnixTimeConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			DateTime dateTime = Convert.ToDateTime(value);

			writer.WriteValue(dateTime.ToUnixTimestamp());
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var timestamp = (long)reader.Value;

			return timestamp.FromUnixTimestamp();
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime);
		}
	}
}