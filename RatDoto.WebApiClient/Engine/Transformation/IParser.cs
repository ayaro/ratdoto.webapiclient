﻿using System.IO;

namespace RatDoto.WebApiClient.Engine.Transformation
{
	public interface IParser
	{
		TResult Parse<TResult>(Stream stream);
	}
}
