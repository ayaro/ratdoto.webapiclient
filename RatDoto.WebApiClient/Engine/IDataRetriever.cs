﻿using System.Collections.Specialized;
using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Engine
{
    public interface IDataRetriever
    {
        Task<TResponse> GetResponse<TResponse>(string relativePath, NameValueCollection parameters);
    }
}