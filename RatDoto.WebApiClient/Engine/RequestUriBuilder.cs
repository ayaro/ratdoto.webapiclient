﻿using System;
using System.Collections.Specialized;
using System.Web;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Engine
{
    public class RequestUriBuilder
    {
        private readonly IApiSettings _apiSettings;

        public RequestUriBuilder(IApiSettings apiSettings)
        {
            if (null == apiSettings)
            {
                throw new ArgumentNullException("apiSettings");
            }

            _apiSettings = apiSettings;
        }

        public Uri BuildRequestUri(string relativePath, NameValueCollection parameters)
        {
            var uriBuilder = new UriBuilder(_apiSettings.Host)
            {
                Path = relativePath,
                Query = createQueryString(parameters)
            };

            return uriBuilder.Uri;
        }

        private string createQueryString(NameValueCollection parameters)
        {
            var collection = HttpUtility.ParseQueryString(string.Empty);

            collection.Add("key", _apiSettings.Key);
            collection.Add("format", "json");
            collection.Add("language", _apiSettings.Language);
            collection.Add(parameters);

            return collection.ToString();
        }
    }
}