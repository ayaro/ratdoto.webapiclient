﻿using RatDoto.WebApiClient.Fluent.Abstract;

namespace RatDoto.WebApiClient
{
    public interface IDotaApi
    {
        IGetMatchHistoryQueryBuilder GetMatchHistory();
        IGetMatchHistoryBySequenceNumberQueryBuilder GetMatchHistoryBySequenceNumber();
        IGetMatchDetailsQueryBuilder GetMatchDetails();
        IGetHeroesQueryBuilder GetHeroes();
        IGetLeagueListingsQueryBuilder GetLeagueListings();
    }
}
