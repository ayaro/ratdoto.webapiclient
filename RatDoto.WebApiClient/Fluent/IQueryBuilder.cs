﻿using System.Threading.Tasks;
using RatDoto.WebApiClient.Queries;

namespace RatDoto.WebApiClient.Fluent
{
    public interface IQueryBuilder<TResult>
    {
        IQuery<TResult> Query { get; }
        TResult Execute();
        Task<TResult> Lazy();
    }
}