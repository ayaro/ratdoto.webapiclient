﻿using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Concrete
{
    public class GetMatchHistoryBySequenceNumberQueryBuilder : QueryBuilderBase<MatchHistoryBySequenceNumber>, IGetMatchHistoryBySequenceNumberQueryBuilder
    {
        private readonly GetMatchHistoryBySequenceNumberQuery _query = new GetMatchHistoryBySequenceNumberQuery();

        internal GetMatchHistoryBySequenceNumberQueryBuilder(IQueryProcessor queryProcessor)
            : base(queryProcessor)
        {
        }

        public override IQuery<MatchHistoryBySequenceNumber> Query
        {
            get { return _query; }
        }

        public IGetMatchHistoryBySequenceNumberQueryBuilder StartingAt(long startAtMatchSequenceNumber)
        {
            _query.StartAtMatchSequenceNumber = startAtMatchSequenceNumber;
            return this;
        }

        public IGetMatchHistoryBySequenceNumberQueryBuilder Take(int matchesRequested)
        {
            _query.MatchesRequested = matchesRequested;
            return this;
        }
    }
}