﻿using System;
using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Concrete
{
    public class GetMatchHistoryQueryBuilder : QueryBuilderBase<MatchHistory>, IGetMatchHistoryQueryBuilder
    {
        private readonly GetMatchHistoryQuery _query = new GetMatchHistoryQuery();

        internal GetMatchHistoryQueryBuilder(IQueryProcessor queryProcessor)
            : base(queryProcessor)
        {
        }

        public override IQuery<MatchHistory> Query
        {
            get { return _query; }
        }

        public IGetMatchHistoryQueryBuilder ForHero(int heroId)
        {
            _query.HeroId = heroId;

            return this;
        }

        public IGetMatchHistoryQueryBuilder WithSkill(Skill skill)
        {
            _query.Skill = skill;

            return this;
        }

        public IGetMatchHistoryQueryBuilder From(DateTime @from)
        {
            _query.MinimumDate = @from;

            return this;
        }

        public IGetMatchHistoryQueryBuilder To(DateTime to)
        {
            _query.MaximumDate = to;

            return this;
        }

        public IGetMatchHistoryQueryBuilder Between(DateTime @from, DateTime to)
        {
            _query.MinimumDate = @from;
            _query.MaximumDate = to;

            return this;
        }

        public IGetMatchHistoryQueryBuilder WithMinimumNumberOfPlayers(int numberOfPlayers)
        {
            _query.MinimumPlayers = numberOfPlayers;

            return this;
        }

        public IGetMatchHistoryQueryBuilder ForAccount(long accountId)
        {
            _query.AccountId = accountId;

            return this;
        }

        public IGetMatchHistoryQueryBuilder ForLeague(int leagueId)
        {
            _query.LeagueId = leagueId;

            return this;
        }

        public IGetMatchHistoryQueryBuilder StartAtMatch(int matchId)
        {
            _query.StartAtMatchId = matchId;

            return this;
        }

        public IGetMatchHistoryQueryBuilder Take(int count)
        {
            _query.MatchesRequested = count;

            return this;
        }

        public IGetMatchHistoryQueryBuilder TournamentGamesOnly()
        {
            _query.TournamentGamesOnly = true;

            return this;
        }
    }
}