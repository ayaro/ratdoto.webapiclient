﻿using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Concrete
{
    public class GetMatchDetailsQueryBuilderQueryBuilder : QueryBuilderBase<MatchDetails>, IGetMatchDetailsQueryBuilder
    {
        private readonly GetMatchDetailsQuery _query;

        internal GetMatchDetailsQueryBuilderQueryBuilder(IQueryProcessor queryProcessor)
            : base(queryProcessor)
        {
            _query = new GetMatchDetailsQuery();
        }

        public override IQuery<MatchDetails> Query
        {
            get { return _query; }
        }

        public IGetMatchDetailsQueryBuilder ForMatch(int matchId)
        {
            _query.MatchId = matchId;
            return this;
        }
    }
}