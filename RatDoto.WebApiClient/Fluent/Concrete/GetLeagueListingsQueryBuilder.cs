﻿using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Concrete
{
    internal class GetLeagueListingsQueryBuilder : QueryBuilderBase<DotaLeagues>, IGetLeagueListingsQueryBuilder
    {
        private readonly IQuery<DotaLeagues> _query = new GetLeagueListingQuery();

        public GetLeagueListingsQueryBuilder(IQueryProcessor queryProcessor) : base(queryProcessor)
        {
        }

        public override IQuery<DotaLeagues> Query
        {
            get { return _query; }
        }
    }
}