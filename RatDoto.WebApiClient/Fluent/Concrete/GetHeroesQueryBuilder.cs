﻿using RatDoto.WebApiClient.Fluent.Abstract;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Concrete
{
    public class GetHeroesQueryBuilder : QueryBuilderBase<DotaHeroes>, IGetHeroesQueryBuilder
    {
        private readonly GetHeroesQuery _query = new GetHeroesQuery();

        internal GetHeroesQueryBuilder(IQueryProcessor queryProcessor)
            : base(queryProcessor)
        {
        }

        public override IQuery<DotaHeroes> Query
        {
            get { return _query; }
        }

        public IGetHeroesQueryBuilder ItemizedOnly()
        {
            _query.ItemizedOnly = true;
            return this;
        }
    }
}