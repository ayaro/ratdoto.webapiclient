﻿using System.Threading.Tasks;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Abstract
{
    public interface IGetLeagueListingsQueryBuilder : IQueryBuilder<DotaLeagues>
    {
    }
}