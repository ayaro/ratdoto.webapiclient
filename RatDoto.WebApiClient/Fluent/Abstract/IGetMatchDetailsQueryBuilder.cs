﻿using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Abstract
{
    public interface IGetMatchDetailsQueryBuilder : IQueryBuilder<MatchDetails>
    {
        IGetMatchDetailsQueryBuilder ForMatch(int matchId);
    }
}