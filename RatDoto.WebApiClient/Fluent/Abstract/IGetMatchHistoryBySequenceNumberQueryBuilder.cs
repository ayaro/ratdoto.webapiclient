﻿using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Abstract
{
    public interface IGetMatchHistoryBySequenceNumberQueryBuilder : IQueryBuilder<MatchHistoryBySequenceNumber>
    {
        IGetMatchHistoryBySequenceNumberQueryBuilder StartingAt(long startAtMatchSequenceNumber);
        IGetMatchHistoryBySequenceNumberQueryBuilder Take(int matchesRequested);
    }
}