﻿using System;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Abstract
{
    public interface IGetMatchHistoryQueryBuilder : IQueryBuilder<MatchHistory>
    {
        IGetMatchHistoryQueryBuilder ForAccount(long accountId);
        IGetMatchHistoryQueryBuilder ForHero(int heroId);
        IGetMatchHistoryQueryBuilder ForLeague(int leagueId);
        IGetMatchHistoryQueryBuilder WithSkill(Skill skill);
        IGetMatchHistoryQueryBuilder From(DateTime from);
        IGetMatchHistoryQueryBuilder To(DateTime to);
        IGetMatchHistoryQueryBuilder Between(DateTime from, DateTime to);
        IGetMatchHistoryQueryBuilder WithMinimumNumberOfPlayers(int numberOfPlayers);
        IGetMatchHistoryQueryBuilder StartAtMatch(int matchId);
        IGetMatchHistoryQueryBuilder Take(int count);
        IGetMatchHistoryQueryBuilder TournamentGamesOnly();
    }
}