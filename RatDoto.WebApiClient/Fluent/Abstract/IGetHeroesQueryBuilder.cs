﻿
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Fluent.Abstract
{
    public interface IGetHeroesQueryBuilder : IQueryBuilder<DotaHeroes>
    {
        IGetHeroesQueryBuilder ItemizedOnly();
    }
}