using System;
using System.Threading.Tasks;
using RatDoto.WebApiClient.Queries;

namespace RatDoto.WebApiClient.Fluent
{
    public abstract class QueryBuilderBase<TResult> : IQueryBuilder<TResult>
    {
        private readonly IQueryProcessor _queryProcessor;

        public abstract IQuery<TResult> Query { get; }

        protected QueryBuilderBase(IQueryProcessor queryProcessor)
        {
            if (null == queryProcessor)
            {
                throw new ArgumentNullException("queryProcessor");
            }

            _queryProcessor = queryProcessor;
        }

        public TResult Execute()
        {
            return _queryProcessor.Execute(Query);
        }

        public Task<TResult> Lazy()
        {
            return _queryProcessor.Lazy(Query);
        }
    }
}