﻿namespace RatDoto.WebApiClient.Settings
{
    public interface IDotaSettings
    {
        string GameId { get; }
    }
}