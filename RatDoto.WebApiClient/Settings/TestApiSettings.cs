namespace RatDoto.WebApiClient.Settings
{
    internal class TestApiSettings : IApiSettings, IDotaSettings
    {
        private readonly CommonSettings _commonSettings;
        private const string DefaultGameId = "205790";
        private const string DefaultLanguage = "en_us";

        public TestApiSettings(CommonSettings commonSettings)
        {
            _commonSettings = commonSettings;
        }

        public string Host
        {
            get { return _commonSettings.Host; }
        }

        public string Key
        {
            get { return _commonSettings.ApiKey; }
        }

        public string GameId
        {
            get { return DefaultGameId; }
        }

        public string Language
        {
            get { return DefaultLanguage; }
        }
    }
}