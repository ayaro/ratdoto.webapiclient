﻿namespace RatDoto.WebApiClient.Settings
{
    internal class ProductionApiSettings : IApiSettings, IDotaSettings
    {
        private readonly CommonSettings _commonSettings;
        private const string DefaultGameId = "570";

        public ProductionApiSettings(CommonSettings commonSettings)
        {
            _commonSettings = commonSettings;
        }

        public string Host
        {
            get { return _commonSettings.Host; }
        }

        public string Key
        {
            get { return _commonSettings.ApiKey; }
        }

        public string GameId
        {
            get { return DefaultGameId; }
        }

        public string Language
        {
            get { return _commonSettings.Language; }
        }
    }
}