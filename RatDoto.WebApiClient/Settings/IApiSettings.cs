﻿
namespace RatDoto.WebApiClient.Settings
{
    public interface IApiSettings
    {
        string Host { get; }
        string Key { get; }
        string Language { get; }
    }
}
