using System;
using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Queries
{
    public abstract class QueryHandlerBase<TRequest, TResponse> : IQueryHandler<TResponse> 
        where TRequest : class, IQuery<TResponse>
    {
        public bool CanProcess(IQuery query)
        {
            return query is TRequest;
        }

        public Task<TResponse> Process(IQuery<TResponse> query)
        {
            var request = query as TRequest;
            if (request == null)
            {
                throw new InvalidOperationException(String.Format("Can't process {0} query with {1} query handler.", query.GetType(), this.GetType()));
            }

            return Process(request);
        }

        protected abstract Task<TResponse> Process(TRequest request);
    }
}