﻿namespace RatDoto.WebApiClient.Queries
{
    public interface IQuery<out TResult> : IQuery
    {
    }

    public interface IQuery
    {
    }
}