﻿using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Queries
{
    public interface IQueryProcessor
    {
        TResult Execute<TResult>(IQuery<TResult> query);
        Task<TResult> Lazy<TResult>(IQuery<TResult> query);
    }
}