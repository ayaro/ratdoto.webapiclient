using System;
using System.Threading.Tasks;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Services;

namespace RatDoto.WebApiClient.Queries
{
    internal class SteamApiQueryHandlerBase<TRequest, TResponse> : QueryHandlerBase<TRequest, TResponse>
        where TRequest : class, IQuery<TResponse>
    {
        private readonly ISteamService<TRequest, Response<TResponse>> _steamService;

        public SteamApiQueryHandlerBase(ISteamService<TRequest, Response<TResponse>> steamService)
        {
            if (null == steamService)
            {
                throw new ArgumentNullException("steamService");
            }

            _steamService = steamService;
        }

        protected async override Task<TResponse> Process(TRequest request)
        {
            Response<TResponse> response = await _steamService.Process(request);
            TResponse responseResult = response.Result;

            return responseResult;
        }
    }
}