﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Queries
{
    internal class QueryProcessor : IQueryProcessor
    {
        private readonly IEnumerable<IQueryHandler> _queryHandlers;

        public QueryProcessor(IEnumerable<IQueryHandler> queryHandlers)
        {
            if (null == queryHandlers)
            {
                throw new ArgumentNullException("queryHandlers");
            }

            _queryHandlers = queryHandlers;
        }

        public TResult Execute<TResult>(IQuery<TResult> query)
        {
            return Lazy(query).Result;
        }

        public Task<TResult> Lazy<TResult>(IQuery<TResult> query)
        {
            if (null == query)
            {
                throw new ArgumentNullException("query");
            }

            var queryHandler = (IQueryHandler<TResult>)_queryHandlers.FirstOrDefault(x => x.CanProcess(query));
            if (queryHandler == null)
            {
                throw new InvalidOperationException(String.Format("No QueryHandler found to process {0} request", query.GetType()));
            }

            return queryHandler.Process(query);
        }
    }
}