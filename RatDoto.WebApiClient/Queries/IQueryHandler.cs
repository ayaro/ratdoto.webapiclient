﻿using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Queries
{
    public interface IQueryHandler<TResponse> : IQueryHandler
	{
		Task<TResponse> Process(IQuery<TResponse> query);
	}

    public interface IQueryHandler
    {
        bool CanProcess(IQuery query);
    }
}