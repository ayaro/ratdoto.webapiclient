﻿using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Requests
{
    public class GetMatchDetailsQuery : IQuery<MatchDetails>
    {
        public int? MatchId { get; set; }
    }
}