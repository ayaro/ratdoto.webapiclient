﻿using System;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Requests
{
    public class GetMatchHistoryQuery : IQuery<MatchHistory>
    {
        public int? HeroId { get; set; }

        public GameMode? GameMode { get; set; }

        public Skill? Skill { get; set; }

        public DateTime? MinimumDate { get; set; }

        public DateTime? MaximumDate { get; set; }

        public int? MinimumPlayers { get; set; }

        public long? AccountId { get; set; }

        public int? LeagueId { get; set; }

        public int? StartAtMatchId { get; set; }

        public int? MatchesRequested { get; set; }

        public bool? TournamentGamesOnly { get; set; }
    }
}