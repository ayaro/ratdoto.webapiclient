﻿
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Responses;

namespace RatDoto.WebApiClient.Requests
{
    public class GetMatchHistoryBySequenceNumberQuery : IQuery<MatchHistoryBySequenceNumber>
    {
        public long? StartAtMatchSequenceNumber { get; set; }

        public int? MatchesRequested { get; set; }
    }
}