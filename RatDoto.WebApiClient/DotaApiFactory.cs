using System.Net;
using Newtonsoft.Json;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Engine.Transformation;
using RatDoto.WebApiClient.Queries;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Services;
using RatDoto.WebApiClient.Services.Dota.Economy;
using RatDoto.WebApiClient.Services.Dota.Matches;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient
{
    public static class DotaApiFactory
    {
        public static IDotaApi Create()
        {
            var jsonSerializer = new JsonSerializer();
            jsonSerializer.Converters.Add(new UnixTimeConverter());

            var webClient = new WebClientWrapper(new WebClient());
            var parser = new JsonParser(jsonSerializer);

            var settings = new ProductionApiSettings(CommonSettings.Default);

            var dataRetriever = new DataRetriever(new RequestUriBuilder(settings), webClient, parser);

            var getMatchHistoryService = new GetMatchHistoryService(dataRetriever, settings);
            var getMatchHistoryBySequenceNumberService = new GetMatchHistoryBySequenceNumberService(dataRetriever, settings);
            var getMatchDetailsService = new GetMatchDetailsService(dataRetriever, settings);
            var getHeroesService = new GetHeroesService(dataRetriever, settings);
            var getLeagueListingsService = new GetLeagueListingsService(dataRetriever, settings);

            var queryHandlerProvider = new QueryProcessor(new IQueryHandler[]
            {
                wrapServiceWithQueryHandler(getMatchHistoryService), 
                wrapServiceWithQueryHandler(getMatchHistoryBySequenceNumberService), 
                wrapServiceWithQueryHandler(getMatchDetailsService),
                wrapServiceWithQueryHandler(getHeroesService),
                wrapServiceWithQueryHandler(getLeagueListingsService)
            });

            return new DotaApi(queryHandlerProvider);
        }

        private static IQueryHandler wrapServiceWithQueryHandler<TRequest, TResponse>(ISteamService<TRequest, Response<TResponse>> steamService)
            where TRequest : class, IQuery<TResponse>
        {
            return new SteamApiQueryHandlerBase<TRequest, TResponse>(steamService);
        }
    }
}