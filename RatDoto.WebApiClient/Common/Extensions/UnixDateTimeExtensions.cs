﻿using System;

namespace RatDoto.WebApiClient.Common.Extensions
{
	internal static class UnixDateTimeExtensions
	{
		private static readonly DateTime UnixEra = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		internal static long? ToUnixTimestamp(this DateTime? dateTime)
		{
		    if (dateTime.HasValue)
		    {
		        return dateTime.Value.ToUnixTimestamp();
		    }

		    return null;
		}

        internal static long ToUnixTimestamp(this DateTime dateTime)
        {
            TimeSpan timestamp = dateTime.Subtract(UnixEra);

            return timestamp.Seconds;
        }

		internal static DateTime FromUnixTimestamp(this long timestamp)
		{
			return UnixEra.AddSeconds(timestamp);
        }
	}
}