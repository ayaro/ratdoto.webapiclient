﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using RatDoto.WebApiClient.Engine;

namespace RatDoto.WebApiClient.Services
{
    public abstract class SteamServiceBase<TRequest, TResponse> : ISteamService<TRequest, TResponse>
    {
        private readonly IDataRetriever _dataRetriever;

        protected SteamServiceBase(IDataRetriever dataRetriever)
        {
            if (null == dataRetriever)
            {
                throw new ArgumentNullException("dataRetriever");
            }

            _dataRetriever = dataRetriever;
        }

        protected abstract string RelativePath { get; }

        protected abstract IEnumerable<ParametersMap> QueryParametersMaps { get; }

        public Task<TResponse> Process(TRequest query)
        {
            var parameters = createQueryParameters(query);
            var response = _dataRetriever.GetResponse<TResponse>(RelativePath, parameters);

            return response;
        }

        private NameValueCollection createQueryParameters(TRequest query)
        {
            var map = new NameValueCollection();

            foreach (var parametersMap in QueryParametersMaps)
            {
                var value = parametersMap.Extractor(query);
                if (value == null)
                {
                    if (parametersMap.IsRequired)
                    {
                        throw new InvalidOperationException(String.Format("Required value '{0}' is not provided.", parametersMap.ParameterName));
                    }
                }
                else
                {
                    map.Add(parametersMap.ParameterName, value.ToString());
                }
            }

            return map;
        }

        protected struct ParametersMap
        {
            private readonly string _parameterName;
            private readonly Func<TRequest, object> _extractor;
            private readonly bool _isRequired;

            public ParametersMap(string parameterName, Func<TRequest, object> extractor, bool isRequired = false)
                : this()
            {
                _parameterName = parameterName;
                _extractor = extractor;
                _isRequired = isRequired;
            }

            public string ParameterName
            {
                get { return _parameterName; }
            }

            public Func<TRequest, object> Extractor
            {
                get { return _extractor; }
            }

            public bool IsRequired
            {
                get { return _isRequired; }
            }
        }
    }
}