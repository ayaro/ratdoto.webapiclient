﻿using System;
using System.Collections.Generic;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Services.Dota.Economy
{
    internal class GetHeroesService : SteamServiceBase<GetHeroesQuery, Response<DotaHeroes>>
    {
        private const string UriFormat = "/IEconDOTA2_{0}/GetHeroes/v1/";
        private readonly IDotaSettings _dotaSettings;

        public GetHeroesService(IDataRetriever dataRetriever, IDotaSettings dotaSettings)
            : base(dataRetriever)
        {
            if (dotaSettings == null)
            {
                throw new ArgumentNullException("dotaSettings");
            }

            _dotaSettings = dotaSettings;
        }

        protected override string RelativePath
        {
            get { return String.Format(UriFormat, _dotaSettings.GameId); }
        }

        protected override IEnumerable<ParametersMap> QueryParametersMaps
        {
            get
            {
                yield return new ParametersMap("itemizedonly", x => x.ItemizedOnly ? 1 : 0);
            }
        }
    }
}