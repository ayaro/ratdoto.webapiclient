﻿using System;
using System.Collections.Generic;
using System.Linq;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Services.Dota.Matches
{
    internal class GetLeagueListingsService : SteamServiceBase<GetLeagueListingQuery, Response<DotaLeagues>>
    {
        private const string UriFormat = "/IDOTA2Match_{0}/GetLeagueListing/v1/";
        private readonly IDotaSettings _dotaSettings;

        public GetLeagueListingsService(IDataRetriever dataRetriever, IDotaSettings dotaSettings)
            : base(dataRetriever)
        {
            if (dotaSettings == null)
            {
                throw new ArgumentNullException("dotaSettings");
            }

            _dotaSettings = dotaSettings;
        }

        protected override string RelativePath
        {
            get { return String.Format(UriFormat, _dotaSettings.GameId); }
        }

        protected override IEnumerable<ParametersMap> QueryParametersMaps
        {
            get { return Enumerable.Empty<ParametersMap>(); }
        }
    }
}
