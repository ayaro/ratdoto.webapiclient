﻿using System;
using System.Collections.Generic;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Services.Dota.Matches
{
    internal class GetMatchHistoryBySequenceNumberService : SteamServiceBase<GetMatchHistoryBySequenceNumberQuery, Response<MatchHistoryBySequenceNumber>>
    {
        private const string UriFormat = "/IDOTA2Match_{0}/GetMatchHistoryBySequenceNum/v1/";
        private readonly IDotaSettings _dotaSettings;

        public GetMatchHistoryBySequenceNumberService(IDataRetriever dataRetriever, IDotaSettings dotaSettings)
            : base(dataRetriever)
        {
            if (dotaSettings == null)
            {
                throw new ArgumentNullException("dotaSettings");
            }

            _dotaSettings = dotaSettings;
        }

        protected override string RelativePath
        {
            get { return String.Format(UriFormat, _dotaSettings.GameId); }
        }

        protected override IEnumerable<ParametersMap> QueryParametersMaps
        {
            get
            {
                yield return new ParametersMap("start_at_match_seq_num", x => x.StartAtMatchSequenceNumber);
                yield return new ParametersMap("matches_requested", x => x.MatchesRequested);
            }
        }
    }
}