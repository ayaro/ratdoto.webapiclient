﻿using System;
using System.Collections.Generic;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Services.Dota.Matches
{
    internal class GetMatchDetailsService : SteamServiceBase<GetMatchDetailsQuery, Response<MatchDetails>>
    {
        private const string UriFormat = "/IDOTA2Match_{0}/GetMatchDetails/v1/";
        private readonly IDotaSettings _dotaSettings;

        public GetMatchDetailsService(IDataRetriever dataRetriever, IDotaSettings dotaSettings)
            : base(dataRetriever)
        {
            if (dotaSettings == null)
            {
                throw new ArgumentNullException("dotaSettings");
            }

            _dotaSettings = dotaSettings;
        }

        protected override string RelativePath
        {
            get { return String.Format(UriFormat, _dotaSettings.GameId); }
        }

        protected override IEnumerable<ParametersMap> QueryParametersMaps
        {
            get
            {
                yield return new ParametersMap("match_id", x => x.MatchId, true);
            }
        }
    }
}