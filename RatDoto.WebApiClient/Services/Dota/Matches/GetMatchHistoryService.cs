﻿using System;
using System.Collections.Generic;
using RatDoto.WebApiClient.Common.Extensions;
using RatDoto.WebApiClient.Engine;
using RatDoto.WebApiClient.Requests;
using RatDoto.WebApiClient.Responses;
using RatDoto.WebApiClient.Settings;

namespace RatDoto.WebApiClient.Services.Dota.Matches
{
    internal class GetMatchHistoryService : SteamServiceBase<GetMatchHistoryQuery, Response<MatchHistory>>
    {
        private const string UriFormat = "/IDOTA2Match_{0}/GetMatchHistory/v1/";
        private readonly IDotaSettings _dotaSettings;

        public GetMatchHistoryService(IDataRetriever dataRetriever, IDotaSettings dotaSettings)
            : base(dataRetriever)
        {
            if (dotaSettings == null)
            {
                throw new ArgumentNullException("dotaSettings");
            }

            _dotaSettings = dotaSettings;
        }

        protected override string RelativePath
        {
            get { return String.Format(UriFormat, _dotaSettings.GameId); }
        }

        protected override IEnumerable<ParametersMap> QueryParametersMaps
        {
            get
            {
                yield return new ParametersMap("account_id", x => x.AccountId);
                yield return new ParametersMap("game_mode", x => x.GameMode);
                yield return new ParametersMap("hero_id", x => x.HeroId);
                yield return new ParametersMap("league_id", x => x.LeagueId);
                yield return new ParametersMap("matches_requested", x => x.MatchesRequested);
                yield return new ParametersMap("date_max", x => x.MaximumDate.ToUnixTimestamp());
                yield return new ParametersMap("date_min", x => x.MinimumDate.ToUnixTimestamp());
                yield return new ParametersMap("min_players", x => x.MinimumPlayers);
                yield return new ParametersMap("skill", x => x.Skill);
                yield return new ParametersMap("start_at_match_id", x => x.StartAtMatchId);
                yield return new ParametersMap("tournament_games_only", x => x.TournamentGamesOnly); ;
            }
        }
    }
}