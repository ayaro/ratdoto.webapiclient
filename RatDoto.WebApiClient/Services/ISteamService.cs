﻿using System.Threading.Tasks;

namespace RatDoto.WebApiClient.Services
{
    public interface ISteamService<in TQuery, TResult>
    {
        Task<TResult> Process(TQuery query);
    }
}
